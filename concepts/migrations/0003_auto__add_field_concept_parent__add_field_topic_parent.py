# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Concept.parent'
        db.add_column(u'concepts_concept', 'parent',
                      self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='concept_children', null=True, to=orm['concepts.Topic']),
                      keep_default=False)

        # Removing M2M table for field parent on 'Concept'
        db.delete_table(db.shorten_name(u'concepts_concept_parent'))

        # Adding field 'Topic.parent'
        db.add_column(u'concepts_topic', 'parent',
                      self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='topic_children', null=True, to=orm['concepts.Subject']),
                      keep_default=False)

        # Removing M2M table for field parent on 'Topic'
        db.delete_table(db.shorten_name(u'concepts_topic_parent'))


    def backwards(self, orm):
        # Deleting field 'Concept.parent'
        db.delete_column(u'concepts_concept', 'parent_id')

        # Adding M2M table for field parent on 'Concept'
        m2m_table_name = db.shorten_name(u'concepts_concept_parent')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('concept', models.ForeignKey(orm[u'concepts.concept'], null=False)),
            ('topic', models.ForeignKey(orm[u'concepts.topic'], null=False))
        ))
        db.create_unique(m2m_table_name, ['concept_id', 'topic_id'])

        # Deleting field 'Topic.parent'
        db.delete_column(u'concepts_topic', 'parent_id')

        # Adding M2M table for field parent on 'Topic'
        m2m_table_name = db.shorten_name(u'concepts_topic_parent')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('topic', models.ForeignKey(orm[u'concepts.topic'], null=False)),
            ('subject', models.ForeignKey(orm[u'concepts.subject'], null=False))
        ))
        db.create_unique(m2m_table_name, ['topic_id', 'subject_id'])


    models = {
        u'concepts.concept': {
            'Meta': {'object_name': 'Concept'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'concept_children'", 'null': 'True', 'to': u"orm['concepts.Topic']"}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'concepts.subject': {
            'Meta': {'object_name': 'Subject'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'subject_children'", 'null': 'True', 'to': u"orm['concepts.Subject']"}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'concepts.topic': {
            'Meta': {'object_name': 'Topic'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'topic_children'", 'null': 'True', 'to': u"orm['concepts.Subject']"}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        }
    }

    complete_apps = ['concepts']