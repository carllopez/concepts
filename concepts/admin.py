from django.contrib import admin
from treeadmin.admin import TreeAdmin

from concepts.models import Tag


class TagAdmin(TreeAdmin):
    pass


admin.site.register(Tag, TagAdmin)